package gentestdata

import java.io.{File, FileOutputStream, OutputStreamWriter}

import scala.util.Random

/**
  * Created by rok on 15/05/16.
  */
object GenerateRandomEnglishText {
  def main(args: Array[String]) {
    val A = ('A' to 'Z').toList
    val rand = new Random()
    var genome: StringBuilder = new StringBuilder

    val sizes = List(
      100000,
      200000,
      400000,
      800000,
      1600000,
      3200000,
      6400000,
      12800000,
      25600000,
      51200000,
      102400000,
      204800000,
      409600000,
      819200000
    )

    for (n <- sizes) {
      val osw = new OutputStreamWriter(new FileOutputStream(new File(s"lib/testdata/eng$n.text")))
      for (i <- 0 to n) {
        genome.append(A(rand.nextInt(A.length)))
        if (i % 10000 == 0) {
          osw.append(genome.toString())
          genome = new StringBuilder
        }
      }
      osw.close()
    }
  }
}
