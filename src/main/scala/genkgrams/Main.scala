package genkgrams

/**
  * Created by rok on 08/05/16.
  */

object Main {
  def main(args: Array[String]) {
    if (args.length != 5) {
      println("Wrong number of arguments")
      System.exit(1)
    }
    // Initial data
    val k = args(0).toInt
    val range = args(1).toCharArray.toList
    val outFile = args(2)
    val name = args(3)
    val algorithmVersion = args(4).toInt

    if (algorithmVersion == 0) new GenerateKGrams(k, range, name).generate(outFile)
    else if (algorithmVersion == 1) new GenerateKGramsImproved(k, range, name).generate(outFile)
    else if (algorithmVersion == 2) new GenerateKGramsPy(k, range, name).generate(outFile)
    else println("Invalid algorithm version")
  }
}