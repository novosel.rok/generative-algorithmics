#include <stdio.h>
#include <string.h>
#include "kgrams.h"
#include "automatons/count2gramsABCD.h"

int main(int argc, char const *argv[]) {
  char* input = "ABCDABD";
  count_kgrams(A, input);

  for (int i = 0; i < A.state_count; i++) {
    if (A.result[i] != 0 && strlen(A.state_names[i]) == 2) {
      printf("Gram: %s, Count: %d\n",
        A.state_names[i], A.result[i]);
    }
  }
  return 0;
}
