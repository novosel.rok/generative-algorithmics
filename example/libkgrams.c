#include <stdio.h>
#include <stdlib.h>
#include "kgrams.h"

void count_kgrams(kgrams_t d, char* s)
{
    int state = 0;
    for (char* p = s; *p != 0; p++) {
        int next = d.char_map[(unsigned char)*p];
        if (next < 0) {
        	printf("ERROR: Illegal char %c\n", *p);
        	fflush(stdout);
        	exit(1);
        }
        state = d.transitions[state * d.alphabet_size + next];
        d.result[state]++;
    }
}