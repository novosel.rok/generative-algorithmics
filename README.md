# Counting k-grams with finite state automata

## How to compile and run the program in the example directory:

### Compile the library libkgrams.c

```
gcc -O3 -c -Wall -Werror -fpic libkgrams.c
gcc -shared -o libkgrams.so libkgrams.o
```

### Compile count_bigrams.c

```
gcc -O3 -L./ -Wall -o count_bigrams count_bigrams.c -lkgrams
```

### Run it
```
./count_bigrams
```

### Output

```
Gram: AB, Count: 2
Gram: BC, Count: 1
Gram: BD, Count: 1
Gram: CD, Count: 1
Gram: DA, Count: 1
```
