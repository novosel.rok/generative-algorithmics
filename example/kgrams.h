#ifndef GENKGRAMSHEADER_H
#define GENKGRAMSHEADER_H

#include <stdlib.h>

typedef struct _kgrams_t {
	unsigned long alphabet_size;
	unsigned long state_count;
	int* transitions;
	int* char_map;
	char** state_names;
	int* result; /* Counts of individual states */
} kgrams_t;

extern void count_kgrams(kgrams_t d, char* s);

#endif