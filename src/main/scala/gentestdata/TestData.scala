package gentestdata

import java.io.{File, FileOutputStream, OutputStreamWriter}

import scala.collection.immutable.HashMap
import scala.util.Random

/**
  * Created by rok on 08/08/16.
  */
object TestData {
  def main(args: Array[String]) {
    val ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    val rand = new Random()
    val dir = "/Volumes/Elements/data/data%s%d.text"
    val sizes = List(
      100000,
      200000,
      400000,
      800000,
      1600000,
      3200000,
      6400000,
      12800000,
      25600000,
      51200000,
      102400000,
      204800000,
      409600000,
      819200000
    )
    var text: StringBuilder = new StringBuilder

    for (i <- 4 to 10; j <- sizes) {
      val alphabet = ALPHABET.substring(0, i)
      val file = dir.format(alphabet, j)
      val osw = new OutputStreamWriter(new FileOutputStream(new File(file)))
      for (k <- 0 to j) {
        text.append(alphabet(rand.nextInt(i)))
        if (k % 10000 == 0) {
          osw.append(text.toString())
          text = new StringBuilder
        }
      }
      osw.append(text.toString())
      text = new StringBuilder
      osw.close()
    }
  }
}
