package genkgrams

import java.io.PrintWriter
import java.io.File

import scala.collection.mutable.ListBuffer

/**
  * A class for generating k-grams for a specific alphabet
  */
class GenerateKGrams(k: Int, range: List[Char], name: String) {

  val alphabet = range.map(x => x.toString)

  val template =
    """
      |kgrams_t %s = {
      |%d,
      |%d,
      |%s,
      |%s,
      |%s,
      |%s
      |};
    """.stripMargin

  /**
    * generate() method generates the Kgrams struct
    * and writes it to file
    */
  def generate(outfile: String): Unit = {
    val base = alphabet.length
    // Tree calculations
    val states = numOfStates(base, k)
    val leaves = numOfLeaves(base, k)
    // FSM
    val initialRange = 1 until states
    val lastLevel = initialRange.slice(states - 1 - leaves, states)
    val fsm = initialRange ++ List.fill(base)(lastLevel).flatten

    /*val output = template.format(
      name,
      alphabet.length,
      states,
      transitions(base, states),
      getCharMapping(range),
      getNodeNames,
      getResultsArray(states)
    )*/

    val writer = new PrintWriter(new File(outfile))
    writer.append("kgrams_t %s = {\n".format(name))
    writer.append(alphabet.length.toString).append(",\n")
    writer.append(states.toString).append(",\n")
    writer.append(transitions(base, states)).append(",\n")
    writer.append(getCharMapping(range)).append(",\n")

    getNodeNames(writer, states)

    writer.append(getResultsArray(states)).append("\n};")

    writer.close()
  }

  private def numOfStates(base: Int, height: Int): Int = (0 to height).map(x => Math.pow(base, x).toInt).sum

  private def numOfLeaves(base: Int, height: Int): Int = Math.pow(base, height).toInt

  private def getFSMArray(fsm: List[IndexedSeq[Int]]): String = "{%s}".format(fsm.mkString(","))

  private def getCharMapping(range: List[Char]): String = {
    val lb = ListBuffer.fill(128)(-1)
    range.zipWithIndex.foreach {
      case (ch, idx) => lb(ch.toInt) = idx
    }
    "(int[%d]){%s}".format(lb.length, lb.mkString(","))
  }

  private def getNodeNames(writer: PrintWriter, states: Int) = {
    writer.append(s"""(char*[$states]){""""")
    for (i <- 1 to k) {
      val sigmai = permutationsWithRepetitions(alphabet, i)
      writer.append(",")
        .append(sigmai.map(x => x.mkString("")).map(x => s""""$x"""").mkString(","))
    }

    writer.append("},\n")
  }

  /*
  private def transitions(states: Int, base: Int): String = {
    val lastLevelStates = Math.pow(base, k - 1).toInt
    val startOfLastLevel = (Math.pow(base, k).toInt - 1) / (base - 1)
    val startOfSubLastLevel = startOfLastLevel - lastLevelStates
    val indices = for (i <- 0 until states; j <- 0 until base) yield(i, j)
    val transitions = indices.foldLeft(List[Int]()) {
      (delta, indices) => {
        val (i, j) = indices
        if (i < startOfLastLevel) {
          delta ::: List(base * i + j + 1)
        } else {
          delta ::: List(base * (startOfSubLastLevel + (i - startOfLastLevel) % lastLevelStates) + j + 1)
        }
      }
    }

    "(int[%d]){%s}".format(transitions.length, transitions.mkString(","))
  }
  */

  private def transitions(base: Int, states: Int): String = {
    def sum(i: Int) = (Math.pow(base, i + 1).toInt - 1) / (base - 1)
    val startKSub1 = sum(k - 1)
    val finalLevel = Math.pow(base, k).toInt
    val finalLevelTransitions = (0 until finalLevel).map(x => x + startKSub1)
    val transitions = (1 until states) ++ List.fill(base)(finalLevelTransitions).flatten
    "(int[%d]){%s}".format(transitions.length, transitions.mkString(","))
  }

  private def getResultsArray(states: Int) = "(int[%d]){%s}".format(states, List.fill(states)(0).mkString(","))

  def writeToFile(output: String, outfile: String): Unit = {
    val writer = new PrintWriter(new File(outfile))
    writer.write(output)
    writer.close()
  }

  def permutationsWithRepetitions[T](input: List[T], n: Int): List[List[T]] = {
    n match {
      case 1 => for (el <- input) yield List(el)
      case _ => for (el <- input; perm <- permutationsWithRepetitions(input, n - 1)) yield el :: perm
    }
  }
}
