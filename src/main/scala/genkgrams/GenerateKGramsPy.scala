package genkgrams

import java.io.PrintWriter
import java.io.File

import scala.collection.mutable.ListBuffer

/**
  * A class for generating k-grams for a specific alphabet
  */
class GenerateKGramsPy(k: Int, range: List[Char], name: String) {

  val alphabet = range.map(x => x.toString)

  val template =
    """
      |kgrams_t %s = {
      |%d,
      |%d,
      |%d,
      |%s,
      |%s,
      |%s,
      |%s
      |};
    """.stripMargin


  /**
    * generate() method generates the Kgrams struct
    * and writes it to file
    */
  def generate(outfile: String): Unit = {
    val base = alphabet.length
    val leaves = numOfLeaves(base, k)
    val fsm = List.fill(base)(0 until leaves).flatten

    val writer = new PrintWriter(new File(outfile))
    writer.append("from genkgramspy import GenKgrams\n")
    writer.append("%s = GenKgrams()\n".format(name))
    writer.append("k = %d\n".format(k))
    writer.append("al_size = %d\n".format(base))
    writer.append("state_count = %d\n".format(leaves))

    val l = (0 until leaves).mkString(",")
    writer.append(s"transitions = [")
    for (i <- 0 until base) {
      if (i == 0) {
        writer.append(l)
      } else {
        writer.append(",").append(l)
      }
    }

    writer.append("]\n")
    writer.append(getCharMapping(range))
    writer.append(getNodeNames(permutationsWithRepetitions(range, k)))
    writer.append(getResultsArray(leaves))
    writer.append("%s.Init(al_size, transitions, char_map, names, result, state_count, k)\n".format(name))
    writer.close()
  }

  private def numOfStates(base: Int, height: Int): Int = (0 to height).foldLeft(0)((a, i) => a + Math.pow(base, i).toInt)

  private def numOfLeaves(base: Int, height: Int): Int = Math.pow(base, height).toInt

  private def getFSMArray(fsm: List[IndexedSeq[Int]]): String = "{%s}".format(fsm.mkString(","))

  private def getCharMapping(range: List[Char]): String = {
    val lb = ListBuffer.fill(128)(-1)
    range.zipWithIndex.foreach {
      case (ch, idx) => lb(ch.toInt) = idx
    }
    "char_map = [%s]\n".format(lb.mkString(","))
  }

  private def getNodeNames(input: List[List[Char]]): String = {
    val output = input.map(x => x.mkString("")).map(x => s"""b"$x"""").mkString(",")
    s"""names = [$output]\n"""
  }

  private def getResultsArray(states: Int) = "result = %s\n".format(List.fill(states)(0).mkString(","))

  def writeToFile(output: String, outfile: String): Unit = {
    val writer = new PrintWriter(new File(outfile))
    writer.write(output)
    writer.close()
  }

  private def permutationsWithRepetitions[T](input: List[T], n: Int): List[List[T]] = {
    n match {
      case 1 => for (el <- input) yield List(el)
      case _ => for (el <- input; perm <- permutationsWithRepetitions(input, n - 1)) yield el :: perm
    }
  }
}
