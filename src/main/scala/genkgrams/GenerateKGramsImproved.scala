package genkgrams

import java.io.PrintWriter
import java.io.File

import scala.collection.mutable.ListBuffer

/**
  * A class for generating k-grams for a specific alphabet
  */
class GenerateKGramsImproved(k: Int, range: List[Char], name: String) {

  val alphabet = range.map(x => x.toString)

  val template =
    """
      |kgrams_t %s = {
      |%d,
      |%d,
      |%d,
      |%s,
      |%s,
      |%s,
      |%s
      |};
    """.stripMargin


  /**
    * generate() method generates the Kgrams struct
    * and writes it to file
    */
  def generate(outfile: String): Unit = {
    val base = alphabet.length
    val leaves = numOfLeaves(base, k)
    val fsm = List.fill(base)(0 until leaves).flatten

    val writer = new PrintWriter(new File(outfile))
    writer.append("kgrams_t %s = {\n".format(name))
    writer.append(k.toString).append(",\n")
    writer.append(base.toString).append(",\n")
    writer.append(leaves.toString).append(",\n")

    //writer.append(transitions(fsm)).append(",\n")
    val l = (0 until leaves).mkString(",")
    writer.append(s"(int[${leaves * base}]){")
    for (i <- 0 until base) {
      if (i == 0) {
        writer.append(l)
      } else {
        writer.append(",").append(l)
      }
    }
    writer.append("},\n")

    writer.append(getCharMapping(range)).append(",\n")

    writer.append(getNodeNames(permutationsWithRepetitions(range, k))).append(",\n")

    writer.append(getResultsArray(leaves)).append("\n};")

    writer.close()
  }

  private def numOfStates(base: Int, height: Int): Int = (0 to height).foldLeft(0)((a, i) => a + Math.pow(base, i).toInt)

  private def numOfLeaves(base: Int, height: Int): Int = Math.pow(base, height).toInt

  private def getFSMArray(fsm: List[IndexedSeq[Int]]): String = "{%s}".format(fsm.mkString(","))

  private def getCharMapping(range: List[Char]): String = {
    val lb = ListBuffer.fill(128)(-1)
    range.zipWithIndex.foreach {
      case (ch, idx) => lb(ch.toInt) = idx
    }
    "(int[%d]){%s}".format(lb.length, lb.mkString(","))
  }

  private def getNodeNames(input: List[List[Char]]): String = {
    val output = input.map(x => x.mkString("")).map(x => s""""$x"""").mkString(",")
    s"""(char*[${input.length}]){$output}"""
  }

  private def transitions(fsm: List[Int]): String = "(int[%d]){%s}".format(fsm.length, fsm.mkString(","))

  private def getResultsArray(states: Int) = "(int[%d]){%s}".format(states, List.fill(states)(0).mkString(","))

  def writeToFile(output: String, outfile: String): Unit = {
    val writer = new PrintWriter(new File(outfile))
    writer.write(output)
    writer.close()
  }

  private def permutationsWithRepetitions[T](input: List[T], n: Int): List[List[T]] = {
    n match {
      case 1 => for (el <- input) yield List(el)
      case _ => for (el <- input; perm <- permutationsWithRepetitions(input, n - 1)) yield el :: perm
    }
  }
}
